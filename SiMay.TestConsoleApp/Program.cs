﻿using SiMay.Sockets.Tcp;
using SiMay.Sockets.Tcp.TcpConfiguration;
using System;
using System.Linq;
using System.Net;
using System.Text;

namespace SiMay.TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var serverConfig = new TcpSocketSaeaServerConfiguration();

            serverConfig.AppKeepAlive = true;
            serverConfig.PendingConnectionBacklog = 0;

            var server = TcpSocketsFactory.CreateServerAgent(TcpSocketSaeaSessionType.Packet, serverConfig, (notify, session) =>
            {
                switch (notify)
                {
                    case TcpSocketCompletionNotify.OnConnected:
                        var data = Encoding.UTF8.GetBytes("你好鸭client");
                        Console.WriteLine($"OnConnected lenght{data.Length} :" + string.Join(",", data.Select(c => c.ToString())));
                        session.SendAsync(data);
                        break;
                    case TcpSocketCompletionNotify.OnSend:
                        break;
                    case TcpSocketCompletionNotify.OnDataReceiveing:
                        break;
                    case TcpSocketCompletionNotify.OnDataReceived:
                        Console.WriteLine("len:" + session.CompletedBuffer.Length + " msg:" + Encoding.UTF8.GetString(session.CompletedBuffer));
                        break;
                    case TcpSocketCompletionNotify.OnClosed:
                        Console.WriteLine("OnClosed");
                        break;
                    default:
                        break;
                }

            });
            server.Listen(new IPEndPoint(IPAddress.Parse("0.0.0.0"), 5200));
            Console.ReadLine();
            Console.WriteLine("Hello World!");
        }
    }
}
